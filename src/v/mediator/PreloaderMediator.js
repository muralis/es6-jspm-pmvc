import AppConstants from "src/AppConstants";
import WindowSizeProxy from "src/m/proxy/WindowSizeProxy";

import Preloader from "src/v/component/Preloader";

class PreloaderMediator extends puremvc.Mediator{

    constructor(){
        super();
        
        this.windowSizeProxy = null;
    }

    listNotificationInterests(){
        return [
            AppConstants.ASSET_LOAD_BEGIN,
            AppConstants.ASSET_LOAD_COMPLETE
        ];
    }

    onRegister(){
        this.setViewComponent(new Preloader());
        this.windowSizeProxy = this.facade.retrieveProxy(WindowSizeProxy.NAME);
    }

    // Handle notifications from other PureMVC actors
    handleNotification(note){
        switch (note.getName()){
            case AppConstants.ASSET_LOAD_COMPLETE:
                this.viewComponent.hide();
                break;
            case AppConstants.ASSET_LOAD_BEGIN:
                this.viewComponent.init(this.windowSizeProxy.windowSizeVO);
                break;
        }
    }
    
}

PreloaderMediator.NAME = "PreloaderMediator";

export default PreloaderMediator;