import AppConstants from "src/AppConstants";
import WindowSizeProxy from "src/m/proxy/WindowSizeProxy";
import ConfigProxy from "src/m/proxy/ConfigProxy";

import Info from "src/v/component/Info";

class InfoMediator extends puremvc.Mediator{

	constructor(){
        super();
        
        this.windowSizeProxy = null;
    }

    listNotificationInterests(){
        return  [
                    AppConstants.WINDOW_RESIZED,
                    AppConstants.ASSET_LOAD_COMPLETE,
                    AppConstants.SERVER_INIT
                ];
    }

    onRegister(){
        this.setViewComponent(new Info());

        this.windowSizeProxy = this.facade.retrieveProxy(WindowSizeProxy.NAME);
        this.configProxy = this.facade.retrieveProxy(ConfigProxy.NAME);
    }

    // Handle notifications from other PureMVC actors
    handleNotification(note){
        switch (note.getName()){
            case AppConstants.SERVER_INIT:
                this.viewComponent.showID(note.getBody());
                break;
            case AppConstants.WINDOW_RESIZED:
                this.viewComponent.handleResize(note.getBody());
                break;
            case AppConstants.ASSET_LOAD_COMPLETE:
                this.viewComponent.init(this.windowSizeProxy.windowSizeVO, this.configProxy.uiConfigVO);
                break;
        }
    }
    
}

InfoMediator.NAME = "InfoMediator";

export default InfoMediator;