import AppConstants from "src/AppConstants";
import WindowSizeProxy from "src/m/proxy/WindowSizeProxy";

import BG from "src/v/component/BG";

class BGMediator extends puremvc.Mediator{

    constructor(){
        super();
        
        this.windowSizeProxy = null;
    }

    listNotificationInterests(){
        return  [
                    AppConstants.WINDOW_RESIZED,
                    AppConstants.ASSET_LOAD_COMPLETE
                ];
    }

    onRegister(){
        this.setViewComponent(new BG());

        this.windowSizeProxy = this.facade.retrieveProxy(WindowSizeProxy.NAME);
    }

    // Handle notifications from other PureMVC actors
    handleNotification(note){
        switch (note.getName()){
            case AppConstants.WINDOW_RESIZED:
                this.viewComponent.handleResize(note.getBody());
                break;
            case AppConstants.ASSET_LOAD_COMPLETE:
                this.viewComponent.init(note.getBody(), this.windowSizeProxy.windowSizeVO);
                break;
        }
    }
    
}

BGMediator.NAME = "BGMediator";

export default BGMediator;