class Info{

	constructor(){
        this.stage = new PIXI.Container();
        this.txtID = null;
    }

    init(windowSizeVO, uiConfig){
        this.addChildren(uiConfig);
        this.setupView(windowSizeVO);

        PXRoot.addChild(this.stage);
    }

    addChildren(uiConfig){
		this.txtID = new PIXI.Text("");
        this.txtID.style = {
            fontFamily: uiConfig.fontFamily,
            fontSize: uiConfig.fontSize,
            fill: uiConfig.fillColor,
            align: uiConfig.align,
            stroke: uiConfig.stroke,
            strokeThickness: uiConfig.strokeThickness
        };
        this.txtID.anchor.set(0.5, 0.5);

        this.stage.addChild(this.txtID);
    }

    showID(data){
        this.txtID.text = data.ID;
    }

    setupView(windowSizeVO){
        this.txtID.x = windowSizeVO.width/2;
        this.txtID.y = windowSizeVO.height/2;
    }

    handleResize(windowSizeVO){
        this.setupView(windowSizeVO);
    }
    
}

export default Info;