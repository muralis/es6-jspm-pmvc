import Utils from "src/m/lib/Utils";

class BG{

    constructor(){
        this.stage = new PIXI.Container();
        this.bg = null;
    }

    init(resources, windowSizeVO){
        this.addChildren(resources);
        this.setupView(windowSizeVO);

        PXRoot.addChild(this.stage);
    }

    addChildren(resources){
        this.bg = new PIXI.Sprite(resources.bg.texture);
        this.bg.anchor.set(0.5,0.5);
        this.stage.addChild(this.bg);
    }

    setupView(windowSizeVO){
        // Fill screen
        var size = Utils.getSizeToFillScreen(
            {
                width:this.bg.width,
                height: this.bg.height
            },
            {
                width:windowSizeVO.width,
                height: windowSizeVO.height
            }
        );

        this.bg.width = size.width;
        this.bg.height = size.height;

        this.bg.x = windowSizeVO.width/2;
        this.bg.y = windowSizeVO.height/2;
    }

    handleResize(windowSizeVO){
        this.setupView(windowSizeVO);
    }
    
}

export default BG;