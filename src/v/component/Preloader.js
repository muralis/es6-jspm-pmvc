
class Preloader{

    constructor(){
        this.stage = new PIXI.Container();

        this.preloader = null;
        this.loadComplete = null;
    }

    init(windowSizeVO){
        this.preloader = new PIXI.Graphics();
        this.preloader.beginFill(0xFFFFFF);
        this.preloader.drawRect(0, 0, windowSizeVO.width * 0.1, windowSizeVO.height * 0.01);
        this.stage.pivot.set(this.preloader.width/2, this.preloader.height/2);
        this.stage.x = windowSizeVO.width/2;
        this.stage.y = windowSizeVO.height/2;
        this.stage.addChild(this.preloader);

        PXRoot.addChild(this.stage);

        requestAnimationFrame(this.rotate.bind(this));
    }

    rotate(){
        if(this.loadComplete){
            // PXRoot.removeChild(this.stage);
        }else{
            this.stage.rotation += 0.1;
            requestAnimationFrame(this.rotate.bind(this));
        }
    }

    hide(){
        this.loadComplete = true;
    }
    
}

export default Preloader;