import Utils from "src/m/lib/Utils";

class WindowSizeVO{
    constructor(w, h){
        this.update(w, h);
    }

    update(w, h){
        this.width = w;
        this.height = h;
        this.orientation = Utils.getOrientation(w, h);
        console.log(this);
    }
}

export default WindowSizeVO;