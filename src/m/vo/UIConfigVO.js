class UIConfigVO{
    
    constructor(){
        this.fontFamily =		"Verdana";
        this.fontSize =			24;
        this.fillColor =		0xFFFFFF;
        this.align =			"center";
        this.stroke =			"black";
        this.strokeThickness =	5;
    }
    
}

export default UIConfigVO;