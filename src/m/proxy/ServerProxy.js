import AppConstants from "src/AppConstants";
import ResultVO from "src/m/vo/ResultVO";
import ServerService from "src/m/proxy/service/ServerService";

class ServerProxy extends puremvc.Proxy{
    
    constructor(){
        super();

        this.resultVO = null;
        this.server = null;
    }

    onRegister(){
        this.resultVO = new ResultVO();
        this.server = new ServerService();
    }

    init(){
        this.server.init(this.onServerInit.bind(this));
    }

    onServerInit(result){
        this.resultVO.update(result);
        this.sendNotification(AppConstants.SERVER_INIT, this.resultVO);
    }

}

ServerProxy.NAME = "ServerProxy";

export default ServerProxy;