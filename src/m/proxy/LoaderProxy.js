import AppConstants from "src/AppConstants";

class LoaderProxy extends puremvc.Proxy{

    constructor(){
        super();

        this.loader = null;
    }

    onRegister(){

    }

    loadAssets(){
        this.loader = new PIXI.loaders.Loader("",3);
        this.loader.add('bg', 'assets/bg.jpg');
        this.loader.load(this.onGraphicsLoadComplete.bind(this));

        this.sendNotification(AppConstants.ASSET_LOAD_BEGIN, this.sound);
    }

    onGraphicsLoadComplete(loader, resources){
        this.sendNotification(AppConstants.ASSET_LOAD_COMPLETE, resources);
    }

}

LoaderProxy.NAME = "LoaderProxy";

export default LoaderProxy;