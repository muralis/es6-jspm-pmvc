// Server simulator on the client
class ServerService{

    constructor(){
        this.callback = null;
    }

    init(callback){
        this.callback = callback;
        setTimeout(this.sendInitResult.bind(this), 500);
    }

    sendInitResult(){
        // Returning data with random ID for sample response
        this.callback({ID: "CL" + Math.ceil(Math.random() * 20000)});
    }
    
}

export default ServerService;