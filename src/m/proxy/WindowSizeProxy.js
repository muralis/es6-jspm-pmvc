import AppConstants from "src/AppConstants";
import WindowSizeVO from "src/m/vo/WindowSizeVO";

class WindowSizeProxy extends puremvc.Proxy{

    constructor(){
        super();

        this.windowSizeVO = null;
    }

    onRegister(){
        this.windowSizeVO = new WindowSizeVO(window.innerWidth, window.innerHeight);

        if(window.addEventListener){
            window.addEventListener("resize", this.onResize.bind(this), true);
        }else{
            if(window.attachEvent){
                window.attachEvent("onresize", this.onResize.bind(this));
            }
        }
    }

    onResize(){
        this.windowSizeVO.update(window.innerWidth, window.innerHeight);
        this.sendNotification(AppConstants.WINDOW_RESIZED, this.windowSizeVO);
    }
    
}
        
WindowSizeProxy.NAME = "WindowSizeProxy";

export default WindowSizeProxy;