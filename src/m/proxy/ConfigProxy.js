import GameConfigVO from "src/m/vo/GameConfigVO";
import UIConfigVO from "src/m/vo/UIConfigVO";

class ConfigProxy extends puremvc.Proxy{

    constructor(){
    	super();

        this.gameConfigVO = null;
        this.uiConfigVO = null;
    }

    onRegister(){
        this.gameConfigVO = new GameConfigVO();
        this.uiConfigVO = new UIConfigVO();
    }
    
}

ConfigProxy.NAME = "ConfigProxy";

export default ConfigProxy;