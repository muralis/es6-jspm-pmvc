import ORIENTATION from "src/m/enum/ORIENTATION";

class Utils{

    static getOrientation(width, height){
        return  (width > height) ?
                    ORIENTATION.LANDSCAPE:
                    ORIENTATION.PORTRAIT;
    }

    /**
     * Provides dimensions of content to fill whole area of the screen
     * without disturbing the aspect ratio
     * @param content
     * @param screen
     * @returns {object}
     */
    static getSizeToFillScreen(content, screen){
        if((screen.width/screen.height) > (content.width/content.height)){
            return  {
                        width: screen.width,
                        height: content.height * (screen.width/content.width)
                    };
        }
        else{
            return  {
                width: content.width * (screen.height/content.height),
                height: screen.height
            };
        }
    }

    /**
     * Provides dimensionss of content to fit whole area of the screen
     * wihtout disturbing the aspect ratio.
     * @param {Object} fitObj - Object with data to apply fit
     * @param {Object}
     */
    static getSizeToFitScreen(content, screen){
        if((screen.width/screen.height) > (content.width/content.height)){
            return  {
                width: content.width * (screen.height/content.height),
                height: screen.height
            };
        }
        else{
            return  {
                width: screen.width,
                height: content.height * (screen.width/content.width)
            };
        }
    }

    // Alternative to jQuery $(document).ready
    static whenDocumentReady(fn){
        if (document.readyState !== 'loading'){
            fn();
        } else {
            document.addEventListener('DOMContentLoaded', fn);
        }
    }
    
}

export default Utils;