import AppConstants from "src/AppConstants";
import StartupCommand from "src/c/command/StartupCommand";
import LoaderProxy from "src/m/proxy/LoaderProxy";
import Utils from "src/m/lib/Utils";

class App{

	constructor(name){
		this.facade = puremvc.Facade.getInstance(name);
		
        this.initializePixi();

		this.facade.registerCommand(AppConstants.STARTUP, StartupCommand);
        this.facade.sendNotification(AppConstants.STARTUP);

        var loaderProxy = this.facade.retrieveProxy(LoaderProxy.NAME);
        loaderProxy.loadAssets();
	}

    initializePixi(){
        PXRoot = new PIXI.Container();

        PXRenderer = new PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight);
        PXRenderer.view.style.display = "block";

        PXRoot.interactive = true;
        PXRoot.on("tap", this.setFullScreen);

        document.getElementById("game").appendChild(PXRenderer.view);

        // Render loop
        window.renderLoop = () => {
            PXRenderer.render(PXRoot);
            requestAnimationFrame(window.renderLoop);
        };
        window.renderLoop();
    }

    setFullScreen(){
        if (screenfull.enabled && !screenfull.isFullscreen){
            screenfull.request();
        }
    }

}

export default App;

Utils.whenDocumentReady(() => {
    console.log("%c " + AppConstants.CORE_NAME + " ","background: #FFD700;");
    window.PXRoot = null;
    window.PXRenderer = null;
    var gecoApp = new App(AppConstants.CORE_NAME);
});
    
