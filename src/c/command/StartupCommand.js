import PrepModelCommand from "src/c/command/PrepModelCommand";
import PrepControllerCommand from "src/c/command/PrepControllerCommand";
import PrepViewCommand from "src/c/command/PrepViewCommand";

class StartupCommand extends puremvc.MacroCommand{

    initializeMacroCommand(){
        this.addSubCommand(PrepControllerCommand);
        this.addSubCommand(PrepModelCommand);
        this.addSubCommand(PrepViewCommand);
    }
    
}

export default StartupCommand;