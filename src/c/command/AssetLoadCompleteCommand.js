import ServerProxy from "src/m/proxy/ServerProxy";

class AssetLoadCompleteCommand extends puremvc.SimpleCommand{

    execute(note){
        var server = this.facade.retrieveProxy(ServerProxy.NAME);
        server.init();
    }
    
}

export default AssetLoadCompleteCommand;