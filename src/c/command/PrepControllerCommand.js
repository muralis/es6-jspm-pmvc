import AppConstants from "src/AppConstants";
import AssetLoadCompleteCommand from "src/c/command/AssetLoadCompleteCommand";
import WindowResizeCommand from "src/c/command/WindowResizeCommand";

class PrepControllerCommand extends puremvc.SimpleCommand{

    execute(note){
        this.facade.registerCommand(AppConstants.ASSET_LOAD_COMPLETE, AssetLoadCompleteCommand);
        this.facade.registerCommand(AppConstants.WINDOW_RESIZED, WindowResizeCommand);
    }
    
}

export default PrepControllerCommand;