class WindowResizeCommand extends puremvc.SimpleCommand{

    execute(note){
        var windowSizeVO = note.getBody();
        PXRenderer.resize(windowSizeVO.width, windowSizeVO.height);
    }

}

export default WindowResizeCommand;