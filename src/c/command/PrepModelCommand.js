import LoaderProxy from "src/m/proxy/LoaderProxy";
import WindowSizeProxy from "src/m/proxy/WindowSizeProxy";
import ConfigProxy from "src/m/proxy/ConfigProxy";
import ServerProxy from "src/m/proxy/ServerProxy";

class PrepModelCommand extends puremvc.SimpleCommand{

    execute(note){
        this.facade.registerProxy(new LoaderProxy());
        this.facade.registerProxy(new WindowSizeProxy());
        this.facade.registerProxy(new ConfigProxy());
        this.facade.registerProxy(new ServerProxy());
    }
    
}

export default PrepModelCommand;