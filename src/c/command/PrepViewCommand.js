import PreloaderMediator from "src/v/mediator/PreloaderMediator";
import BGMediator from "src/v/mediator/BGMediator";
import InfoMediator from "src/v/mediator/InfoMediator";

class PrepViewCommand extends puremvc.SimpleCommand{

    execute(note){
        this.facade.registerMediator(new PreloaderMediator());
        this.facade.registerMediator(new BGMediator());
        this.facade.registerMediator(new InfoMediator());
    }
    
}

export default PrepViewCommand;