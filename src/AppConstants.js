class AppConstants{

}

AppConstants.CORE_NAME = 			"ScratchCity";

AppConstants.STARTUP = 				"AppConstants_startup";
AppConstants.ASSET_LOAD_BEGIN = 	"AppConstants_asset_load_begin";
AppConstants.ASSET_LOAD_COMPLETE = 	"AppConstants_asset_load_complete";
AppConstants.WINDOW_RESIZED = 		"AppConstants_window_resized";
AppConstants.SERVER_INIT = 			"AppConstants_server_init";

export default AppConstants;