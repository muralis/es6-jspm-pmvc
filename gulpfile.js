var public_folder =     "public/";
    html_template =     "src/index-template.html";
    bundle_filename =   "app.js";
    bundle_dirname =    public_folder + "js/";

var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint');
    browserSync = require('browser-sync').create();
    jspm = require("jspm");
    fs = require("fs");

var browser_sync_options = {
        reloadOnRestart: true,
        open: (gutil.env.o == undefined ? false : true),
        server: {
            baseDir: public_folder,
            ghostMode: true
        }
    }




// == jspm build ==>>>
var map_filename = bundle_filename + ".map";
    bundle = bundle_dirname + bundle_filename;
    map = bundle_dirname + map_filename;
    builder = new jspm.Builder();
gulp.task('build', function() {
    createIndex(insertBundleTag);
    builder.buildStatic(
        "src/App.js", 
        {
            sourceMaps: true,
            minify: true,
            mangle: true
        }
    ).then(markAndWrite)
});
function markAndWrite(output){
    var d = new Date();
    var bundleMark = 'console.log("%c Geco BUNDLE ","background: #00BB00; fff; padding:2px 0;");\n';
    var mapComment = "//# sourceMappingURL=" + map_filename;

    output.source = bundleMark + output.source + mapComment;

    fs.writeFile(bundle, output.source, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("** Bundle saved to " + bundle);
    });
    fs.writeFile(map, output.sourceMap, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("** Sourcemap saved to " + map);
    });
    startBrowserWithBundle();
}
function startBrowserWithBundle(){
    browserSync.init(browser_sync_options);
}
function getTimeStamp(){
    var st = "";
    var d = new Date();
    st =    d.getFullYear() + 
            ("0" + (d.getMonth() + 1)).slice(-2) + 
            d.getDate() + 
            ("0" + d.getHours()).slice(-2) + 
            ("0" + d.getMinutes()).slice(-2) + 
            ("0" + d.getSeconds()).slice(-2);
    return st;
}
// <<<== jspm build ==




// == jshint ==>>>
var jshintConfig = {
    "lookup": false,
    "curly": true,
    "eqeqeq": true,
    "esversion": 6,
    "forin": true,
    "freeze": true,
    "futurehostile": true,
    "latedef": "nofunc",
    "maxcomplexity": 20,
    "maxdepth": 3,
    "maxparams": 4,
    "nocomma": false,
    "nonew": true,
    "notypeof": true,
    "strict": "implied",
    "undef": false,
    "unused": false,
    "browser": true,
    "devel": true,
    "globals": {
        "puremvc": true,
        "PXRoot": true,
        "PXRenderer": true,
        "PIXI": true,
        "screenfull": true
    }
}
gulp.task('jshint', function() {
    gutil.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    return gulp.src('src/**/*.js')
        .pipe(jshint(jshintConfig))
        .pipe(jshint.reporter('jshint-stylish'));
});
// <<<== jshint ==




// == update ==>>>
gulp.task('update', function () {
    createIndex(insertDevTags, continueUpdate);
});
function continueUpdate(){
    browserSync.reload();
}
// <<<== update ==




gulp.task('default', ["jshint"], function() {
    createIndex(insertDevTags);
    browser_sync_options.server.routes = {
                "/jspm_packages": "jspm_packages",
                "/src": "src",
                "/config.js": "config.js"
            }
    browserSync.init(browser_sync_options);
    gulp.watch(
            ['src/**/*.js','src/**/*.html'],
            ["jshint", "update"]
        )
});




// == index parsers ==>>>
function createIndex(fn, cb){
    cb = cb == undefined ? function(){} : cb;
    var index_file = public_folder + "index.html";
    fs.readFile(html_template, "utf-8", function(err, data){
        if (err) return console.log(err);
        new_data = fn(data);
        fs.readFile(index_file, "utf-8", function(err, data){
            if (err){
                index_data = "";
            }else{
                index_data = data;
            }
            if(new_data == index_data){
                console.log("** No changes in index file");
                cb();
            }else{
                fs.writeFile(index_file, new_data, function(err) {
                    if(err) return console.log(err);
                    console.log("** Index file created ");
                    cb();
                }); 
            }
        });
    });
}
function insertDevTags(data){
    var code =  '<!-- JSPM Runtime style is for development. Production html will include self-executing bundle -->\n' +
                '   <script src="jspm_packages/system.js"></script>\n' +
                '   <script src="config.js"></script>\n' +
                '   <script>\n' +
                '       console.log("%c Geco DEV ","background: #FFA500; fff; padding:2px 0;");\n' +
                '       System.import("src/App");\n' +
                '   </script>\n';
    return data.replace("<!--GENERATE-JSPM-TAGS-->", code);
}
function insertBundleTag(data){
    var code =  '<!-- JSPM App -->\n' +
                '   <script src="js/app.js"></script>\n';
    return data.replace("<!--GENERATE-JSPM-TAGS-->", code);
}
// <<<== index parsers ==